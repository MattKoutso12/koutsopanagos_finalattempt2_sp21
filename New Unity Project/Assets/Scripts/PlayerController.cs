﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;
    public CharacterController controller;
    public GameObject humanForme;
    public GameObject snakeForme;
    public float jumpForce;
    public float gravityScale;

    private bool isHuman = true;
    private bool isSnake = false;
    private Vector3 moveDirection;

    // Start is called before the first frame update
    void Start()
    { 
        morph();
    }

    // Update is called once per frame
    void Update()
    {
        float yStore = moveDirection.y;
        moveDirection = (transform.forward * Input.GetAxis("Vertical") * moveSpeed) + (transform.right * Input.GetAxis("Horizontal") * moveSpeed);
        moveDirection = moveDirection.normalized * moveSpeed;
        moveDirection.y = yStore;

        if (controller.isGrounded)
        {
            moveDirection.y = 0f;
            if (Input.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpForce;
            }
        }

        if(Input.GetButtonDown("Fire1"))
        {
            morph();
        }

        moveDirection.y += Physics.gravity.y * gravityScale;

        controller.Move(moveDirection * Time.deltaTime);
    }

    private void morph()
    {
        if(isHuman)
        {
            controller = humanForme.GetComponent<CharacterController>();
            controller.enabled = false;
            humanForme.transform.position = snakeForme.transform.position;
            humanForme.transform.rotation = snakeForme.transform.rotation;
            GameController.forme = humanForme;
            humanForme.SetActive(true);
            snakeForme.SetActive(false);
            isHuman = false;
            isSnake = true;
            GameController.isHuman = true;
            GameController.isSnake = false;
            controller.enabled = true;
        }
        else if(isSnake)
        {
            controller = snakeForme.GetComponent<CharacterController>();
            controller.enabled = false;
            snakeForme.transform.position = humanForme.transform.position;
            snakeForme.transform.rotation = humanForme.transform.rotation;
            GameController.forme = snakeForme;
            snakeForme.SetActive(true);
            humanForme.SetActive(false);
            isSnake = false;
            isHuman = true;
            GameController.isSnake = true;
            GameController.isHuman = false;
            controller.enabled = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Key"))
        {
            Destroy(other.gameObject);
            GameController.hasKey = true;
        }
    }
}
