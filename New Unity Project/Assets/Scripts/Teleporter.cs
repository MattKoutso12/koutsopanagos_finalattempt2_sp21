﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public Transform waypoint;


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Snake"))
        {
            other.gameObject.GetComponent<CharacterController>().enabled = false;
            Vector3 destination = new Vector3(waypoint.position.x, waypoint.position.y, waypoint.position.z);
            other.gameObject.transform.position = destination;
            other.gameObject.GetComponent<CharacterController>().enabled = true;
        }
    }
}
